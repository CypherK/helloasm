package ch.ethz.koradir.helloasm.instrumentation_targets;

public class HelloWorld {
    public String doSomething(){
        String output = "Hello World";
        System.out.println(output);
        return output;
    }
}